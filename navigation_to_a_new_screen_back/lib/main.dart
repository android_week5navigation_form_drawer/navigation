import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    home: FristRoute(),
  ));
}

class FristRoute extends StatelessWidget {
  const FristRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Frist Route'),
      ),
      body: Center(
          child: Column(
        children: [
          SizedBox(height: 10),
          ElevatedButton(
            child: Text('Open route 2'),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return SecondRoute();
              }));
            },
          ),
          SizedBox(height: 10),
          ElevatedButton(
            child: Text('Open route 3'),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return ThirdRoute();
              }));
            },
          ),
        ],
      )),
    );
  }
}

class SecondRoute extends StatelessWidget {
  const SecondRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Frist Route'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Go back '),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

class ThirdRoute extends StatelessWidget {
  const ThirdRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Third Route'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Go back '),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
